# vue3-mui

<p align="center">
    <a href="https://www.npmjs.com/package/vue3-mui">
        <img src="https://img.shields.io/npm/v/vue3-mui.svg" alt="Version">
    </a>
    <a href="https://www.npmjs.com/package/vue3-mui">
        <img src="https://img.shields.io/npm/dt/vue3-mui.svg" alt="Downloads">
    </a>
    <a href="https://www.npmjs.com/package/vue3-mui">
        <img src="https://img.shields.io/bundlephobia/min/vue3-mui.svg" alt="Bundle Size">
    </a>
    <a href="https://www.npmjs.com/package/vue3-mui">
        <img src="https://img.shields.io/npm/l/vue3-mui.svg" alt="License">
    </a>
</p>

vue3-mui is a library of UI components specifically made with Vue.js 3 in mind, which adhere to the <a href="https://material.io/design">Google Material Design</a> specification.

## Installation and Usage

Install vue3-mui using npm or yarn:

``` bash
npm install vue3-mui --save
yarn add vue3-mui
```

Import or require vue3-mui in your code:

``` javascript
import { createApp } from 'vue';
import App from './app.vue';
import vue3Mui from 'vue3-mui';

createApp(App)
    .use(vue3Mui)
    .mount('#app');
```

Or import individual components:

``` javascript
import { defineComponent } from 'vue';
import { uiButton, uiContainer } from 'vue3-mui';

export default defineComponent({
    components: {
        uiButton,
        uiContainer,
    }
});
```

Then use in your template:

``` html
<template>
    <ui-container>
        <ui-button>It works!</ui-button>
    </ui-container>
</template>
```
## Components in dev

- [x] Alert
- [x] App
- [x] App bar
- [x] Avatar
- [ ] Breadcrumb
- [x] Badge
- [x] Banner
- [x] Button
- [x] Card
- [x] Container
- [ ] Checkbox
- [ ] Chip
- [ ] Collapse
- [ ] Dialogs
- [x] Divider
- [ ] Dropdown
- [x] Hover
- [ ] Images
- [x] Icon
- [ ] Input
- [ ] List
- [ ] Loading
- [x] Main
- [x] Navigation drawer
- [ ] Navbar
- [ ] Notification
- [ ] Number Input
- [ ] Pagination
- [ ] Popup
- [ ] Progress
- [ ] Radio
- [ ] Select
- [ ] Sidebar
- [ ] Slider
- [x] Switch
- [x] Table
- [ ] Tabs
- [ ] Textarea
- [ ] Tooltip
- [ ] Upload

## Contributing

This project is in development, so all contributions are welcomed.

## License

MIT
