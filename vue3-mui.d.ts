import { DefineComponent, Plugin } from "vue";
import {Icon} from './src/types/Icon'

declare const Vue3Mui: Exclude<Plugin["install"], undefined>;
export default Vue3Mui;

export const uiAlert: DefineComponent<
  {
    title: string;
    dismissible: boolean;
    type: string;
    textColor: string;
    elevation: number;
    borderRadius: number;
  }>;
export const uiApp: DefineComponent<{}, {}, any>;
export const uiAppBar: DefineComponent<{}, {}, any>;
export const uiAvatar: DefineComponent<{}, {}, any>;
export const uiButton: DefineComponent<{}, {}, any>;
export const uiCard: DefineComponent<{}, {}, any>;
export const uiCardContent: DefineComponent<{}, {}, any>;
export const uiCardHeader: DefineComponent<{}, {}, any>;
export const uiContainer: DefineComponent<{}, {}, any>;
export const uiDivider: DefineComponent<{}, {}, any>;
export const uiIcon: DefineComponent<{
    name: string,
    type: string,
    fill: boolean,
    weight: number,
    grade: number,
    size: number,
}>;
