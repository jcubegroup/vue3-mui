import { ObjectDirective } from "vue";

// @ts-ignore
const handleRipple = (element: any, binding: any, ev: any) => {
  const rippleElement = document.createElement("span");
  const diameter = Math.max(element.clientWidth, element.clientHeight);
  const radius = diameter / 2;

  rippleElement.style.width = rippleElement.style.height = `${diameter}px`;
  rippleElement.style.left = `${ev.clientX - element.offsetLeft - radius}px`;
  rippleElement.style.top = `${ev.clientY - element.offsetTop - radius}px`;
  rippleElement.classList.add("md-ripple");

  const ripple = element.getElementsByClassName("md-ripple")[0];

  if (ripple) {
    ripple.remove();
  }

  element.appendChild(rippleElement);
};

const vRipple: ObjectDirective = {
  mounted: (el: any, binding: any) => {
    el.style.position = "relative";
    el.style.overflow = "hidden";
    el.addEventListener("click", (ev: any) => handleRipple(el, binding, ev));
  },
};

export default vRipple;
