import { App, Plugin } from "vue";

// Import vue components
import * as components from "@/components/index";
// import * as directives from "@/directives/index";
import clickOutside from '@/directives/click-outside'
import ripple from '@/directives/ripple'

// install function executed by Vue.use()
const install: Exclude<Plugin["install"], undefined> = function installVue3Mui(
  app: App
) {
  app.directive("click-outside", clickOutside);
  app.directive("ripple", ripple);
  // Object.entries(directives).forEach(([directiveName, directive]) => {
  //   app.directive(directiveName, directive);
  // });
  Object.entries(components).forEach(([componentName, component]) => {
    app.component(componentName, component);
  });
};

// Create module definition for Vue.use()
export default install;

// To allow individual component use, export components
// each can be registered via Vue.component()
export * from "@/components/index";
