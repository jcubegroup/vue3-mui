import { onMounted, onUnmounted, ref } from "vue";

export const breakpoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  xxl: 1400,
  mobile: 992,
};

export function useBreakpoints() {
  const isMobile = ref();

  const update = () => {
    isMobile.value = window.innerWidth <= breakpoints["mobile"];
  };

  onMounted(() => {
    isMobile.value = window.innerWidth <= breakpoints["mobile"];
    window.addEventListener("resize", update);
  });

  onUnmounted(() => {
    window.removeEventListener("resize", update);
  });

  return {
    isMobile,
  };
}
