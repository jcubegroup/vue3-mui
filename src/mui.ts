import { ref } from "vue";

const appBar = ref(0);
const navigationDrawer = ref(0);

export function useMui() {
  return {
    appBar,
    navigationDrawer,
  };
}
