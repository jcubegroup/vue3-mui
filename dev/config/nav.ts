export default [
  {
    type: "header",
    title: "Getting Started",
    subtitle: "Get started with MUI",
  },

  {
    title: "Components",
    route: "app",
    icon: "apps",
    children: [
      { title: "App", route: "app" },
      { title: "Alert", route: "alert" },
      // { title: "App Bar", route: "app-bar" },
      // { title: "Icons", route: "icons" },
      { title: "Button", route: "button" },
      // { title: "Card", route: "card" },
      // { title: "Container", route: "" },
      // { title: "Divider", route: "" },
      { title: "Dropdown", route: "dropdown" },
      // { title: "Grid", route: "" },
      // { title: "Loading Spinner", route: "" },
      { title: "Modal", route: "modal" },
      // { title: "Navigation Drawer", route: "" },
      // { title: "Page Header", route: "" },
      // { title: "Switch", route: "switch" },
      { title: "Text Field", route: "text-field" },
      { title: "Table", route: "table" },
    ],
  },
];
