import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/components/app-bar",
  },
  {
    path: "/components/:config",
    name: "components",
    component: () => import(/* webpackChunkName: "core" */ "../components.vue"),
  },
];
// @ts-ignore
const requireRoute = require.context("../pages", true, /router(.*)index.ts$/);

requireRoute.keys().forEach((fileName: any) => {
  routes.push(...requireRoute(fileName).default);
});


const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
