export default {
    componentName: 'Theme',
    componentDescription:
        'An app bar to be displayed at the top of the screen.',
    componentUsage: '<vm-theme mode="dark"></vm-theme>',
    propInfo: [
        {
            name: 'mode',
            type: 'string',
            description: 'The color of the theme.',
            default: 'light',
        },
    ],
    examples: [
    ],
};
