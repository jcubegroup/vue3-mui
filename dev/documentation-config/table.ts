import Basic from "./tabes/basic.vue";

export default {
  componentName: "Table",
  componentDescription:
    "For displaying tabular data, <ui-table> supports pagination, filtering, sorting, custom rendering," +
    " various style options, events, and asynchronous data. For simple display of tabular data without all the fancy features," +
    " BootstrapVue provides two lightweight alternative components <ui-table-lite> and <ui-table-simple>.",
  propInfo: [
    {
      name: "bordered",
      type: "Boolean",
      default: "false",
      description: "Adds borders to all the cells and headers",
    },
    {
      name: "borderless",
      type: "Boolean",
      default: "false",
      description: "Removes all borders from cells",
    },
    {
      name: "busy",
      type: "Boolean",
      default: "false",
      description:
        "When set, forces the table into the busy state.Automatically set when an items provider function is being called",
    },
    {
      name: "caption",
      type: "String",
      default: "",
      description: "Text string to place in the caption element",
    },
    {
      name: "caption-top",
      type: "Boolean",
      default: "false",
      description:
        "Visually place the table caption above the table. Default is below",
    },
    {
      name: "dark",
      type: "Boolean",
      default: "false",
      description: "Places the table in dark mode",
    },
    {
      name: "fields",
      type: "Array",
      default: "null",
      description: "Array of field names or array of field definition objects",
    },
    {
      name: "foot-clone",
      type: "Boolean",
      default: "false",
      description:
        "Enable the footer of the table, and clone the header content by default",
    },
    {
      name: "hover",
      type: "Boolean",
      default: "false",
      description: "Enables hover styling on rows",
    },
    {
      name: "id",
      type: "String",
      default: "false",
      description:
        "Used to set the `id` attribute on the rendered content, and used as the base to generate any additional element IDs as needed",
    },
    {
      name: "items",
      type: "Array or Function",
      default: "[]",
      description:
        "Array of items to display, or an items provider function reference. Refer to the docs for details",
    },
  ],
  examples: [
    {
      title: "Basic usage",
      description: "",
      component: Basic,
    },
  ],
};
