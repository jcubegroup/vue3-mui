import List from "./icons/list.vue";

export default {
    componentName: 'Icons',
    componentDescription:
        '',
    propInfo: [
        {
            name: 'mode',
            type: 'string',
            description: 'The color of the theme.',
            default: 'light',
        },
    ],
    examples: [
        {
            title: "Basic usage",
            description:
                "",
            component: List,
        },
    ],
};
