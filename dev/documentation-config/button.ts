import Basic from "./buttons/Basic.vue";
import Size from "./buttons/Size.vue";
import Icon from "./buttons/Icon.vue";
import Elevation from "./buttons/Elevation.vue";

export default {
  componentName: "Button",
  componentDescription:
    "A regular button which can be used to trigger actions or as a router-link. The text color of a contained button is determined by the lightness of the chosen primary color.",
  propInfo: [
    {
      name: "variant",
      type: "'contained' | 'outlined' | 'text'",
      description: "The variant of button to render.",
      default: "contained",
    },
    {
      name: "routerPath",
      type: "string",
      description:
        "If this value is set, the button will become a router-link and will direct the router to this path.",
      default: "",
    },
    {
      name: "isDisabled",
      type: "boolean",
      description: "If true, the button will be disabled.",
      default: "false",
    },
    {
      name: "isSubmit",
      type: "boolean",
      description: "Whether to treat the button as a submit button for a form.",
      default: "false",
    },
    {
      name: "color",
      type: "string",
      description:
        'The primary color of the button, must in the form "r, g, b".',
      default: "98, 0, 238",
    },
    {
      name: "elevation",
      type: "number",
      description: "Set the elevation level of the button.",
      default: "2",
    },
    {
      name: "isAutoTextColor",
      type: "boolean",
      description: "Automatically determine text color based on primary color.",
      default: "false",
    },
  ],
  examples: [
    {
      title: "Basic usage",
      description:
        "Use color, variant, pill and rounded to define Button's style.",
      component: Basic,
    },
    {
      title: "Buttons Sizes",
      description: "Use size to define Button's size.",
      component: Size,
    },
    {
      title: "Elevation",
      description: "Use elevation to define Button's shadow.",
      component: Elevation,
    },
    {
      title: "With Icon",
      description: "Use size to define Button's size.",
      component: Icon,
    },
  ],
};
