export const tableExample = `<ui-table :items="tableItems">
  <template #head()>
    <ui-skeleton
        type="rect"
        width="100"
        height="18"
        class="mb-1"
    />
  </template>
  <template #cell()>
    <ui-skeleton
        type="rect"
        height="12"
        class="mb-1"
    />
  </template>
</ui-table>
      
<script>
   export default {
      computed: {
        tableItems() {
        return JSON.parse('[' + \`,[${',1'.repeat(5).substr(1)}]\`.repeat(5).substr(1) + ']')
      }
   }
</script> `