export const BasicDrop = `<ui-dropdown text="Dropdown Button" width="450">
    <ui-card>
      <ui-card-content>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab atque,
        dicta earum eius excepturi maiores maxime necessitatibus odio
        perferendis provident rerum, voluptate! Et fugiat itaque libero nam,
        odio odit porro?
      </ui-card-content>
    </ui-card>
</ui-dropdown>`

export const btnSlot = `<ui-dropdown width="450">
  <template #button-content="{ toggle }">
    <div class="d-flex flex-middle">
      <ui-btn variant="icon" @click="toggle"  class="mr-1">
        <ui-icon name="info" />
      </ui-btn>
      <b>Button via html</b>
    </div>
  </template>
  <ui-card>
    <ui-card-content>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab
      atque, dicta earum eius excepturi maiores maxime necessitatibus
      odio perferendis provident rerum, voluptate! Et fugiat itaque
      libero nam, odio odit porro?
    </ui-card-content>
  </ui-card>
</ui-dropdown>`