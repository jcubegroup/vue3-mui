import { createApp } from 'vue';
import App from './app.vue';
import router from './router';
import Vue3Mui from '@/entry.esm';

import '@/styles/theme.scss'
import '@/styles/main.scss'

createApp(App)
    .use(Vue3Mui)
    .use(router)
    .mount('#app');
